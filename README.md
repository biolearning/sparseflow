# Installation

```
conda install numpy matplotlib pandas scipy configargparse scikit-learn ipython
conda install -c conda-forge tensorflow
pip install -e .
```

# Example
Example of modeling ChEMBL dataset. Firstly download the example dataset by
```
wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-346targets.mm
wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-compound-feat.mm
```

Now we can train a SparseFlow model with 100,000 sparse input features.

```python
import sparseflow
import scipy.io
import numpy as np
import logging

## loading data
ic50 = scipy.io.mmread("chembl-IC50-346targets.mm")
ecfp = scipy.io.mmread("chembl-IC50-compound-feat.mm")
ic50.data = (ic50.data > 6.5)

test = np.random.rand(ic50.shape[0]) < 0.2

## using logging to show progress
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

## running factorization (Macau)
smlp = sparseflow.SparseMLP(
         Y            = ic50,
         Ytest        = test,
         X            = ecfp,
         h_sizes      = [100],
         optimizer    = "adam",
         nonlin       = "relu",
         lr           = [1e-3, 3e-4],
         lr_durations = [10, 10],
         batch_size   = 150,
         regul        = 10,
         dropout      = 0.8,
         auc_min_pos  = 50,
         auc_min_neg  = 50,
)
smlp.run()
```

## Predictions
After the model has been trained we use it to make predictions,
either on the training, test sets or on new compounds.

```python
## classification prediction matrix for all training samples
Ytrain_hat = smlp.get_y_train()

## DataFrame with classification predictions for test training points
Ytest_hat = smlp.get_y_testset()

## Make full predictions for the given new input matrix Xnew for all tasks (columns)
Ynew_hat = smlp.predict(Xnew)
```

## Parameters
Here is the explanation for the parameters of `SparseMLP`:
* `h_sizes` is a list of hidden sizes (number of neurons). E.g., `h_sizes=[100]` means one hidden layer with 100 neurons.
* `lr` and `lr_durations` are the learning rates and durations, i.e., how many epochs each learning rate is used.
* `batch_size` specifies the mini-batch size relative to the total dataset or the absolute size:
   * `batch_size = 0.01` means each mini-batch uses 1% of the data.
   * `batch_size = 150` means each mini-batches uses 150 rows.
* `regul` specifies the regularization (total for whole epoch)
   * for L2 regularization use a number, e.g., `regul = 0.1`.
   * for L1 set `regul = tf.contrib.layers.l1_regularizer(0.1)` where 0.1 is the strength of the regularization.
   * for L1+L2 set `regul = tf.contrib.layers.l1_l2_regularizer(0.1, 0.2)` where first number specifies L1 and second L2 regularization. Note the L1 and L2 values do not have to some to 1.0.

For AUC reporting SparseFlow computes an AUC for each task (column of Y) and then computes the average. If `auc_min_pos=25` and `auc_min_neg=25` then only tasks with at least 25 positives and 25 negatives are used for the average.

## Regression with SparseFlow
SparseFlow also supports regression.
Here is an example with the previous dataset where we do not threshold the values to be classes but leave them as real values.
```python
import sparseflow
import scipy.io
import numpy as np
import logging

## using logging to show progress
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

## loading data
ic50 = scipy.io.mmread("chembl-IC50-346targets.mm")
ecfp = scipy.io.mmread("chembl-IC50-compound-feat.mm")

## No thresholding of ic50

test = np.random.rand(ic50.shape[0]) < 0.2

## using logging to show progress
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
smlp = sparseflow.SparseMLP(
         Yregr        = ic50,
         Yregr_test   = test,
         X            = ecfp,
         h_sizes      = [200, 200],
         nonlin       = "tanh",
         optimizer    = "adam",
         lr           = [1e-3, 3e-4, 1e-4],
         lr_durations = [20, 10, 10],
         batch_size   = 150,
         regul        = 10.0,
         dropout      = 0.7,
)
smlp.run()
```

This should get to RMSE from 0.7 to 0.8 on the test set.
Note that we used tanh as the non-linearity (`nonlin="tanh"`), which is known to be better for regression than `relu` (the default non-linearity).

To extract predictions we can use similar functions as for classification:
```python
## regression prediction matrix for all training samples
Ytrain_hat = smlp.get_y_regr_train()

## DataFrame with regression predictions for test training points
Ytest_hat = smlp.get_y_regr_testset()

## Make full predictions for the given new input matrix Xnew for all tasks (columns)
Ynew_hat = smlp.predict_regr(Xnew)
```

