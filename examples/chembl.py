## Download small example data (command line)

#wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-346targets.mm
#wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-compound-feat.mm

## loading data
import numpy as np
import scipy.io
import scipy.sparse
import sparseflow
import logging

ic50 = scipy.io.mmread("chembl-IC50-346targets.mm").tocsr()
ecfp = scipy.io.mmread("chembl-IC50-compound-feat.mm")

## create classification for 5.5 and 6.5 levels
Y55      = ic50.copy()
Y55.data = Y55.data > 5.5
Y65      = ic50.copy()
Y65.data = Y65.data > 6.5

Y = scipy.sparse.hstack( [Y55, Y65] )

## enable logging for seeing evaluations
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


## setting 0.2 of the rows as testing: (
Ytest = np.random.rand(Y.shape[0]) < 0.2

smlp = sparseflow.SparseMLP(
         Y            = Y,
         Ytest        = Ytest,
         X            = ecfp,
         h_sizes      = [100], ## depends on data
         l2_reg       = 1.0,   ## depends on data
         lr           = [1e-3 , 1e-4],
         lr_durations = [15, 5],
         auc_min_pos  = 10,
         auc_min_neg  = 10,
         batch_size   = 0.02)

smlp.run(evaluate=True)

## training X
yhat_train = smlp.get_y_train()

## load new X
x_unlabeled = ecfp ## for example use the same
yhat_new    = smlp.predict(x_unlabeled)

