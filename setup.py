from setuptools import setup

exec(open("sparseflow/version.py").read())

setup(name='sparseflow',
      version=__version__,
      description='Deep matrix factorization in TensorFlow',
      url='http://github.com/jaak-s/sparseflow',
      author='Jaak Simm',
      author_email='jaak.simm@gmail.com',
      packages=['sparseflow'],
      install_requires=["numpy", "scipy", "pandas", "tqdm", "sklearn"],
      zip_safe=False)

