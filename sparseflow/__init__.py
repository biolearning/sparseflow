from .multilayer import SparseMLP, ExtendedSMLP, make_train_test_rows
from .version import __version__
