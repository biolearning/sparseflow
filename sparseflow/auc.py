import sklearn.metrics
import numpy as np
import pandas as pd

def comp_auc(ytrue, ypred):
    if len(ytrue) <= 1:
        return np.nan
    if (ytrue[0] == ytrue).all():
        return np.nan
    fpr, tpr, thresholds = sklearn.metrics.roc_curve(ytrue, ypred)
    return sklearn.metrics.auc(fpr, tpr)

def comp_aucs(cols, ypred, ytrue, auc_cols):
    """ cols     - column values for each Y value
        ypred    - prediction score for each Y score
        ytrue    - true label for each Y
        auc_cols - columns to compute AUC for
    """
    if len(cols) != len(ypred):
       raise ValueError("cols and ypred have to be the same size.")
    if len(cols) != len(ytrue):
       raise ValueError("cols and ytrue have to be the same size.")
    aucs = np.zeros(auc_cols.shape[0])

    for i, col in enumerate(auc_cols):
        idx = np.where(cols==col)[0]
        aucs[i] = comp_auc(ytrue[idx], ypred[idx])

    return aucs

def comp_aucs_pd(cols, ypred, ytrue):
    df   = pd.DataFrame({"col": cols, "ypred": ypred, "ytrue": ytrue})
    aucs = df.groupby("col", sort=True).apply(lambda g:
              comp_auc(ytrue=g.ytrue.values, ypred=g.ypred.values))
    return aucs


