import tensorflow as tf
import numpy as np
import pandas as pd
import numbers
from collections import OrderedDict

import scipy.sparse
from scipy.sparse import hstack, csr_matrix, csc_matrix, coo_matrix
from scipy import sparse
import sklearn.metrics
import os.path
import time
from tqdm import tqdm

## local modules
from .auc import comp_aucs_pd

import logging
from tensorflow.core.framework import summary_pb2


def make_summary(name, val):
    return summary_pb2.Summary(value=[summary_pb2.Summary.Value(tag=name, simple_value=val)])

def make_train_test_rows(Y, row_is_test):
    """Splits Y into two matrices of the original shape."""
    assert row_is_test.dtype == np.bool, "Vector of test indices must be boolean."
    Ycoo  = Y.tocoo()
    test  = row_is_test[Ycoo.row]
    train = (test == False)
    y_csr_train = csr_matrix((Ycoo.data[train], (Ycoo.row[train], Ycoo.col[train])), shape=Y.shape)
    y_csr_test  = csr_matrix((Ycoo.data[test], (Ycoo.row[test], Ycoo.col[test])), shape=Y.shape)
    return y_csr_train, y_csr_test

def to1encoding(Y):
    if Y.dtype == np.bool:
        Y.data = Y.data * 2.0 - 1.0
        return Y
    if (np.abs(Y.data) != 1.0).any():
        return None
    return Y

def select_rows(X, rows = None):
    if rows is None:
        Xtmp = X
    else:
        Xtmp = X[rows, :]
    return np.column_stack(Xtmp.nonzero()), Xtmp.data, Xtmp.shape

def remove_nan(Y):
    if not np.any(np.isnan(Y.data)):
        return Y
    Y   = Y.tocoo(copy = False)
    idx = np.where(np.isnan(Y.data) == False)[0]
    return scipy.sparse.csr_matrix( (Y.data[idx], (Y.row[idx], Y.col[idx])), shape = Y.shape )

class SparseMLP:
    def __init__(self,
                 Y             = None,
                 Ytest         = None,
                 X             = None,
                 Yregr         = None,
                 Yregr_test    = None,
                 Yregr_weight  = 1.0,
                 Yaux          = None,
                 Yaux_weight   = None,
                 Yprime        = None,
                 Yprime_weight = None,
                 squeeze_regul = 0.0,
                 squeeze_level = 0.0,
                 auc_min_pos = 50,
                 auc_min_neg = 50,
                 auc_cols    = None,
                 nonlin      = "relu",
                 h_sizes     = None,
                 input_block = 0,
                 dropout     = 1.0,
                 regul       = 1e-3,
                 l2_reg      = None,
                 #emb_dropout = 1.0,
                 emb_sizes   = None,
                 emb_nonlin  = "relu",
                 emb_regul   = 1e-3,
                 optimizer   = "adam",
                 lr          = [1e-3, 1e-4],
                 lr_durations= [50, 50],
                 batch_size  = 0.01,
                 board       = None,
                 verbose     = False,
        ):
        """
        Y               sparse matrix
        Ytest           sparse matrix of test values or boolean array of test rows (length == Y.shape[0])
        X               sparse matrix (scipy) or dense (numpy)
        Yregr           sparse matrix of regression values (Yregr.shape[0] == X.shape[0])
        Yregr_test      either (1) boolean vector specifying test rows or
                               (2) scipy sparse matrix (CSR/CSC/COO) with test values, same shape as Yregr.
        Yregr_weight    scalar for weighting the cost Yregr matrix
        Yaux            additional classification tasks with values from 0 to 1 (rows correspond to the rows of Y)
        Yaux_weight     weight for the Yaux tasks
        Yprime          (synonym for Yaux)
        Yprime_weight   (synonym for Yaux_weight)
        squeeze_level   splitting features into main and tail part
        squeeze_regul   regularization for squeezed features
        auc_min_pos     number of positives of column for calculating AUC
        auc_min_neg     number of negatives of column for calculating AUC
        auc_cols        columns to compute AUC on (if specified then auc_min_pos, auc_min_neg are not used)
        nonlin          non-linearity ("relu", "relu6", "elu", "tanh")
        h_sizes         hidden layer sizes (a list)
        input_block     max output size of input layer block (for large sparse inputs), 0 means no blocking (default)
        dropout         dropout retain probability
        regul           scalar (for l2 regularization)
                        or tf.contrib.layers.l1/l2/l1_l2_regularizer
        l2_reg          synonym to regul (for backwards compatability)
        #emb_dropout     embedding retain rate
        emb_sizes       embedding sizes
        emb_nonlin      embedding non-linearity ("relu", "elu", "tanh")
        emb_regul       embedding regularization
        optimizer       optimizer ("adam", "sgd", "rmsprop")
        lr              list of learning rates
        lr_durations    list of learning rate durations
        batch_size      size of minibatches, either an int or a float.
                        In the latter case gives the relative size
        board           board directory (None means no board)
        verbose         if True then performance is printed out (otherwise logged) 
        """
        self.verbose = verbose

        if board and (os.path.isdir(board) or os.path.isfile(board)):
            raise ValueError("ERROR: Tensorboard directory already exists: " + board)

        if (Yregr is not None) and (Y is None):
            ## only regression data
            Y = sparse.csr_matrix((X.shape[0], 0))

        if Y.shape[0] != X.shape[0]:
            raise ValueError("Y and X must have the same number of rows.")
        Y = to1encoding(Y)
        if Y is None:
            raise ValueError("Y must contain only booleans or 1.0 / -1.0 values.")

        if (h_sizes is None) or (type(h_sizes) != list):
            raise ValueError("h_sizes should be specified, must be a list of hidden layer sizes.")

        if Ytest is None:
            Ytest = sparse.csr_matrix((Y.shape))
        if type(Ytest) == np.ndarray:
            if Y.shape[0] != len(Ytest):
                raise ValueError("Number of rows in Y must equal the length of Ytest")
            Ytrain, Ytest = make_train_test_rows(Y, Ytest)
        elif type(Ytest) in [scipy.sparse.coo.coo_matrix, scipy.sparse.csr.csr_matrix, scipy.sparse.csc.csc_matrix]:
            Ytrain = Y
            Ytest  = to1encoding(Ytest)
            if Ytest is None:
                raise ValueError("Ytest must contain only booleans or 1.0 / -1.0 values.")
        else:
            raise TypeError("Ytest must be either a sparse matrix or an array of booleans.")

        if Ytrain.shape != Ytest.shape:
            raise ValueError("Y and Ytest must have the same shape.")

        if type(X) == np.ndarray:
            self.X = X
        else:
            self.X = X.tocsr()

        if Yregr is not None:
            if type(Yregr) not in [scipy.sparse.coo.coo_matrix, scipy.sparse.csr.csr_matrix, scipy.sparse.csc.csc_matrix]:
                raise TypeError("Yregr must be a scipy.sparse matrix: coo, csr or csc.")
            if Yregr_weight is None:
                raise ValueError("If Yregr is provided then Yregr_weight must be a number.")
            if Yregr.shape[0] != X.shape[0]:
                raise ValueError("Yregr.shape[0] and X.shape[0] must be equal.")
            if len(Yregr.shape) != 2:
                raise ValueError("Yregr must be 2D array (matrix).")

            if Yregr_test is None:
                Yregr_test = sparse.csr_matrix((Yregr.shape))
            elif type(Yregr_test) == np.ndarray:
                if Yregr.shape[0] != len(Yregr_test) or Yregr_test.dtype != np.bool:
                    raise ValueError("Yregr_test must be a boolean vector with size equal to number of rows of Yregr.")
                Yregr, Yregr_test = make_train_test_rows(Yregr, Yregr_test)
            elif type(Yregr_test) in [scipy.sparse.coo.coo_matrix, scipy.sparse.csr.csr_matrix, scipy.sparse.csc.csc_matrix]:
                if Yregr.shape != Yregr_test.shape:
                    raise ValueError("Yregr and Yregr_test must have equal shapes.")
            else:
                raise TypeError("Yregr_test must be either (1) boolean vector specifying test rows or (2) scipy sparse matrix (CSR/CSC/COO).")
            Yregr      = remove_nan(Yregr).tocsr(copy = False)
            Yregr_test = remove_nan(Yregr_test).tocsr(copy = False)

        self.Yregr        = Yregr
        self.Yregr_test   = Yregr_test
        self.Yregr_weight = Yregr_weight

        self.Yaux = Yaux
        self.Yaux_weight = Yaux_weight
        if self.Yaux is None:
            self.Yaux = Yprime
        if self.Yaux_weight is None:
            self.Yaux_weight = Yprime_weight
        if self.Yaux is not None:
            self.Yaux = self.Yaux.tocsr(copy=False)

            if (self.Yaux.data < 0.0).any() or (1.0 < self.Yaux.data).any():
                raise ValueError("All Yaux values must be within range [0, 1].")
            if self.Yaux_weight is None:
                raise ValueError("If Yaux is provided, also need to pass Yaux_weight.")
            if self.Yaux.shape[0] != Y.shape[0]:
                raise ValueError("Yaux.shape[0] and Y.shape[0] must be equal.")
            if len(self.Yaux.shape) != 2:
                raise ValueError("Yaux must be 2D array (matrix).")

        self.board = board

        self.dropout     = dropout

        ## for backwards compatability
        if l2_reg is not None:
            regul = l2_reg

        if isinstance(regul, numbers.Number):
            self.reg_layer = tf.contrib.layers.l2_regularizer(float(regul))
        elif callable(regul):
            self.reg_layer = regul
        else:
            raise ValueError("Input 'regul' must be either number (L2 regul) or tf.contrib.layers.l1/l2/l1_l2_regularizer specifying the regularization")

        self.optimizer   = optimizer
        #self.epoch      = 0
        self.lr_path     = np.repeat(lr, lr_durations)

        ## separating features to main and tail part
        self.squeeze_level = squeeze_level
        self.squeeze_regul = squeeze_regul

        if squeeze_level > 0:
            raise ValueError("non-zero squeeze_level not implemented.")
            fsizes = X.sum(0)
            self.fmain = fsizes[fsizes >= squeeze_level]
            self.ftail = fsizes[fsizes  < squeeze_level]

        ## convert to CSR (just in case)
        self.Ytrain = Ytrain.tocsr()
        self.Ytest  = Ytest.tocsr()

        ## computing batch_size
        if type(batch_size) == int:
            self.batch_num_rows = batch_size
        elif type(batch_size) == float:
            assert batch_size <= 1.0
            self.batch_num_rows = np.int(batch_size * self.Ytrain.shape[0])
        else:
            raise TypeError("batch_size must an integer or float.")

        self.num_pos = (self.Ytest == 1).sum(0) + (self.Ytrain == 1).sum(0)
        self.num_neg = (self.Ytest ==-1).sum(0) + (self.Ytrain ==-1).sum(0)
        if auc_cols is None:
            self.auc_cols = np.array(np.where(
                (self.num_pos >= auc_min_pos) &
                (self.num_neg >= auc_min_neg)
            )[1]).flatten()
            logging.info("Computing mean AUC for %d columns (with %d positives and %d negatives)." %
                         (len(self.auc_cols), auc_min_pos, auc_min_neg))
        else:
            self.auc_cols = auc_cols
            logging.info("Using the provided %d columns for computing the mean AUC." % len(self.auc_cols))

        nonlins = {"relu":  tf.nn.relu,
                   "relu6": tf.nn.relu6,
                   "elu":   tf.nn.elu,
                   "tanh":  tf.tanh,
                   "swish": lambda x: x * tf.sigmoid(x)}
        if nonlin not in nonlins:
            raise ValueError("nonlin must be one of %s." % nonlins.keys())
        if emb_nonlin not in nonlins:
            raise ValueError("emb_nonlin must be one of %s." % nonlins.keys())

        self.nonlin     = nonlins[nonlin]
        self.h_sizes    = h_sizes
        self.input_block = input_block

        self.emb_nonlin = nonlins[emb_nonlin]
        self.emb_regul  = emb_regul
        self.emb_sizes  = emb_sizes
        self.aucs_train = []
        self.aucs_test  = []
        self.loglosses_train = []
        self.loglosses_test  = []
        self.rmses_train = []
        self.rmses_test = []

        self.setup_tf()

    def is_dense(self):
        return type(self.X) == np.ndarray

    def has_regr(self):
        return self.Yregr is not None

    def has_yaux(self):
        return self.Yaux is not None

    @property
    def epoch(self):
        return self.sess.run(self.global_step)

    def make_input_layer(self, h_size, regul):
        """Creates a linear layer from inputs:
        h_sizes    list of layer sizes
        regul      regularization layer
        """
        W1 = tf.get_variable("W", shape=[self.X.shape[1], h_size], dtype=tf.float32, regularizer = regul)
        b1_init = np.full(int(h_size), 0.1, dtype=np.float32)
        b1 = tf.get_variable("b", dtype=tf.float32, initializer = b1_init)

        if self.is_dense():
            self.x_dense = tf.placeholder(tf.float32, shape=[None, self.X.shape[1]], name="x_dense")
            ## first layer: Dense matmul
            h1 = tf.nn.bias_add(tf.matmul(self.x_dense, W1), b1)
        else:
            x = tf.SparseTensor(indices=self.x_indices, values=self.x_value, dense_shape=self.x_shape)
            ## first layer: SparseDense matmul
            h1 = tf.nn.bias_add(tf.sparse_tensor_dense_matmul(x,W1), b1)
        return h1


    def make_tower(self, h_sizes, nonlin, regul, dropout, embed, input_block=-1):
        """Creates network from inputs:
        h_sizes       list of layer sizes
        nonlin        non-linearity (tf function)
        regul         variable/value for l2 regularization
        dropout       retain probability
        embed         whether to use embedding
        input_block   maximum block size for the first layer (useful for massive sparse input)
        """
        with tf.variable_scope("layer_0"):
            if input_block <= 0 or input_block >= h_sizes[0]:
                h1 = self.make_input_layer(h_sizes[0], regul)
            else:
                ## creates several blocks and concats them
                starts = np.arange(0, h_sizes[0], input_block)
                ends   = np.minimum(starts + input_block, h_sizes[0])
                h1s    = []
                for i, _ in enumerate(starts):
                    with tf.variable_scope("block_{}".format(i)):
                        h1s.append(self.make_input_layer(
                            h_size = ends[i] - starts[i],
                            regul  = regul
                        ))

                h1 = tf.concat(h1s, axis=1)


        if embed:
            h1 = self.embedding(self.cmpd_idx, h1)
        h1 = nonlin(h1)
        hdo = tf.nn.dropout(h1, dropout)

        ## creating hidden layers for levels 2+
        for i, h_size in enumerate(h_sizes[1:], start=1):
            with tf.variable_scope("layer_%d" % i):
                W      = tf.get_variable("W", shape=[hdo.shape[1], h_size], dtype=tf.float32, regularizer = regul)
                b_init = np.full(int(h_size), 0.1, dtype=np.float32)
                b      = tf.get_variable("b", dtype=tf.float32, initializer = b_init)
                h      = nonlin(tf.nn.bias_add(tf.matmul(hdo, W), b))
                hdo    = tf.nn.dropout(h, dropout)
        return hdo

    def setup_tf(self):
        tf.reset_default_graph()

        ## global step (epoch number)
        self.global_step      = tf.Variable(0, dtype=tf.int32, name='global_step', trainable=False)
        self.incr_global_step = tf.assign(self.global_step, self.global_step + 1)

        #Placeholders
        self.cmpd_idx   = tf.placeholder(tf.int64, shape=[None], name="cmpd_idx")

        self.x_indices  = tf.placeholder(tf.int64, shape=[None,2], name="x_indices")
        self.x_value    = tf.placeholder(tf.float32, shape=[None], name="x_value")
        self.x_shape    = tf.placeholder(tf.int64, shape=[2], name="x_shape")

        self.y_true_idx   = tf.placeholder(tf.int64, shape=[None, 2], name="y_true_idx")
        self.y_true_value = tf.placeholder(tf.float32, shape=[None], name="y_true_value")

        self.learning_rate  = tf.placeholder(tf.float32, name="learning_rate")
        self.dropout_retain = tf.placeholder(tf.float32, name="dropout_retain")
        self.num_batches    = tf.placeholder(tf.float32, shape=(), name="num_batches")

        self.last_hidden = self.make_tower(
                h_sizes = self.h_sizes,
                nonlin  = self.nonlin,
                regul   = self.reg_layer,
                dropout = self.dropout_retain,
                embed   = True,
                input_block = self.input_block)

        ## Output layer (classification)
        h_size = self.h_sizes[-1]
        output_size = self.Ytrain.shape[1]
        Wo     = tf.get_variable("Wo", shape=[h_size, output_size], dtype=tf.float32, regularizer = self.reg_layer)
        bo     = tf.get_variable("bo", shape=[output_size], dtype=tf.float32, initializer = tf.zeros_initializer())
        self.y = tf.nn.bias_add(tf.matmul(self.last_hidden, Wo), bo, name="y")


        ## Loss calculation
        y_sel   = tf.gather_nd(self.y, self.y_true_idx, name="y_sel")
        logloss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=y_sel, labels=self.y_true_value))

        loss = logloss

        ## Yregr loss (squared error loss)
        if self.has_regr():
            regr_size = self.Yregr.shape[1]
            self.yregr_true_idx   = tf.placeholder(tf.int64, shape=[None, 2], name="yregr_true_idx")
            self.yregr_true_value = tf.placeholder(tf.float32, shape=[None], name="yregr_true_value")
            #self.yregr_true   = tf.placeholder(tf.float32, shape=[None, regr_size], name="yregr_true")
            #self.yregr_mask   = tf.placeholder(tf.bool, shape=[None], name="yregr_mask")

            Wo_regr = tf.get_variable("Wo_regr", shape=[h_size, regr_size], dtype=tf.float32, regularizer = self.reg_layer)
            bo_regr = tf.get_variable("bo_regr", shape=[regr_size], dtype=tf.float32, initializer = tf.zeros_initializer())
            self.yregr     = tf.nn.bias_add(tf.matmul(self.last_hidden, Wo_regr), bo_regr, name="yregr")
            self.yregr_sel = tf.gather_nd(self.yregr, self.yregr_true_idx, name="yregr_sel")

            ## squared error
            self.yregr_loss = tf.reduce_sum(tf.square(self.yregr_sel - self.yregr_true_value))
            loss += self.Yregr_weight * self.yregr_loss

            self.yregr_size = tf.size(self.yregr_true_value)
            self.yregr_sse  = self.yregr_loss
            self.yregr_mse  = self.yregr_loss / tf.cast(self.yregr_size, tf.float32)
        else:
            ## dummy NaN value when no regression
            self.yregr_sse  = tf.constant(np.nan, dtype=np.float32)
            self.yregr_size = tf.constant(0, dtype=np.int64)
            self.yregr_mse  = tf.constant(np.nan, dtype=np.float32)

        if self.has_yaux():
            yaux_size = self.Yaux.shape[1]

            self.y_prime_idx   = tf.placeholder(tf.int64, shape=[None, 2], name="y_prime_idx")
            self.y_prime_value = tf.placeholder(tf.float32, shape=[None], name="y_prime_value")

            Wo_prime = tf.get_variable("Wo_prime", shape=[h_size, yaux_size], dtype=tf.float32, regularizer = self.reg_layer)
            bo_prime = tf.get_variable("bo_prime", shape=[yaux_size], dtype=tf.float32, initializer = tf.zeros_initializer())
            self.yaux = tf.nn.bias_add(tf.matmul(self.last_hidden, Wo_prime), bo_prime, name="yaux")

            ## Loss calculation
            yaux_sel   = tf.gather_nd(self.yaux, self.y_prime_idx, name="yaux_sel")
            self.yaux_logloss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=yaux_sel, labels=self.y_prime_value))

            loss += self.Yaux_weight * self.yaux_logloss


        ## regularization losses
        reg_term = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        loss    += sum(reg_term) / self.num_batches

        self.y_sel   = y_sel
        self.logloss = logloss
        self.loss    = loss

        if self.optimizer =="adam":
            train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(loss)
        elif self.optimizer == "sgd":
            train_op = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(loss)
        elif self.optimizer == "rmsprop":
            train_op = tf.train.RMSPropOptimizer(self.learning_rate).minimize(loss)
        else:
            raise ValueError("Undefined optimizer: %s" % self.optimizer)
        self.train_op = train_op
        logging.info("TensorFlow model setup completed!")

        ## storing board
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        if self.board:
            self.train_writer = tf.summary.FileWriter(self.board + "/train", sess.graph)
            self.test_writer  = tf.summary.FileWriter(self.board + "/test")

        self.sess = sess

        ## Feed dictionaries: Training set error computation
        fd_train = {}
        self.fd_regularization(fd_train, train=False)
        self.fd_input(fd_train, self.X)
        fd_train[self.cmpd_idx]     = np.arange(self.X.shape[0])

        fd_train[self.y_true_idx]   = np.column_stack(self.Ytrain.nonzero())
        fd_train[self.y_true_value] = (self.Ytrain.data == 1.0).astype(np.float32)
        if self.has_regr():
            fd_train[self.yregr_true_idx]   = np.column_stack(self.Yregr.nonzero())
            fd_train[self.yregr_true_value] = self.Yregr.data

        #Feed dictionaries: Testset error computation
        fd_test = {}
        self.fd_regularization(fd_test, train=False)
        fd_test[self.cmpd_idx]     = fd_train[self.cmpd_idx]
        if self.is_dense():
            fd_test[self.x_dense]  = fd_train[self.x_dense]
        else:
            fd_test[self.x_indices]= fd_train[self.x_indices]
            fd_test[self.x_value]  = fd_train[self.x_value]
            fd_test[self.x_shape]  = fd_train[self.x_shape]
        fd_test[self.y_true_idx]   = np.column_stack(self.Ytest.nonzero())
        fd_test[self.y_true_value] = (self.Ytest.data == 1.0).astype(np.float32)
        if self.has_regr():
            fd_test[self.yregr_true_idx]   = np.column_stack(self.Yregr_test.nonzero())
            fd_test[self.yregr_true_value] = self.Yregr_test.data

        self.fd_train = fd_train
        self.fd_test  = fd_test

    def get_hidden_train(self):
        """returns the last hidden layer matrix for all training samples."""
        return self.sess.run(self.last_hidden, feed_dict=self.fd_train)

    def get_y_train(self):
        """returns classification prediction matrix for all training samples."""
        return self.sess.run(self.y, feed_dict=self.fd_train)

    def get_y_regr_train(self):
        """returns regression prediction matrix for all training samples."""
        return self.sess.run(self.yregr, feed_dict=self.fd_train)

    def get_y_testset(self):
        """returns DataFrame with classification predictions for test training points."""
        ## storing yhat for the testset
        df = pd.DataFrame()
        df["row"] = self.fd_test[self.y_true_idx][:,0]
        df["col"] = self.fd_test[self.y_true_idx][:,1]
        df["y_prob"] = self.sess.run(tf.sigmoid(self.y_sel), feed_dict=self.fd_test)
        df["y_true"] = self.fd_test[self.y_true_value]
        return df

    def get_y_regr_testset(self):
        """returns DataFrame with classification predictions for test training points."""
        ## storing yhat for the testset
        df = pd.DataFrame()
        df["row"]    = self.fd_test[self.yregr_true_idx][:,0]
        df["col"]    = self.fd_test[self.yregr_true_idx][:,1]
        df["y_hat"]  = self.sess.run(self.yregr_sel, feed_dict=self.fd_test)
        df["y_true"] = self.fd_test[self.yregr_true_value]
        return df

    def has_embedding(self):
        return (self.emb_sizes is not None) and (len(self.emb_sizes) > 0)

    def embedding(self, cmpd_idx, h_in):
        if not self.has_embedding():
            return h_in

        if self.emb_sizes[-1] != self.h_sizes[0]:
            raise ValueError("the last embedding size should be equal to the first hidden size of the main network! (%d != %d)" % (self.emb_sizes[-1], self.h_sizes[0]))

        with tf.variable_scope("embedding"): 
            lookup = tf.get_variable("lookup", shape=[self.X.shape[0], self.emb_sizes[0]], dtype = tf.float32)
            h      = tf.gather(lookup, cmpd_idx)
            for i in range(1, len(self.emb_sizes)):
                with tf.variable_scope("emb_layer_%d" % i):
                    #h      = tf.nn.dropout(self.emb_nonlin(h), dropout_retain)
                    h      = self.emb_nonlin(h)
                    h_size = self.emb_sizes[i]
                    h_prev_size = self.emb_sizes[i-1]
                    W      = tf.get_variable("W", shape=[h_prev_size, h_size], dtype=tf.float32)
                    b_init = np.full(int(h_size), 0.1, dtype=np.float32)
                    b      = tf.get_variable("b", dtype=tf.float32, initializer = b_init)
                    hw     = tf.matmul(h, W)
                    tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES, tf.multiply(tf.nn.l2_loss(hw), self.emb_regul * self.num_batches))
                    h      = tf.nn.bias_add(hw, b)
            return h_in + h

    def predict(self, X):
        """Makes full predictions for the given input matrix X for all Y columns."""
        if self.has_embedding():
            raise ValueError("Prediction is not supported on models with embedding.")
        if X.shape[1] != self.X.shape[1]:
            raise ValueError("X.shape[1] (%d) must be equal to original feature size %d." % (X.shape[1], self.X.shape[1]))

        fd = {}
        self.fd_regularization(fd, train=False)
        self.fd_input(fd, X)
        return self.sess.run(self.y, feed_dict=fd)

    def predict_regr(self, X):
        """Makes full *regression* predictions for the given input matrix X for all Yregr columns."""
        if self.has_embedding():
            raise ValueError("Prediction is not supported on models with embedding.")
        if X.shape[1] != self.X.shape[1]:
            raise ValueError("X.shape[1] (%d) must be equal to original feature size %d." % (X.shape[1], self.X.shape[1]))

        fd = {}
        self.fd_regularization(fd, train=False)
        self.fd_input(fd, X)

        return self.sess.run(self.yregr, feed_dict=fd)

    def eval(self, train, bsize = 10000):
        """
        Arguments:
        train   if True then train otherwise test evaluation
        bsize   minibatch size

        Returns a tuple:
        mean logloss
        predictions
        y coordinates (N by 2 matrix)
        y values
        MSE
        """
        if train:
            Y           = self.Ytrain
            Yregr       = self.Yregr
            regr_masker = 0
        else:
            Y           = self.Ytest
            Yregr       = self.Yregr_test
            regr_masker = 1

        logloss   = 0.0
        pred_list = []
        yi_list   = []
        yv_list   = []
        sse       = 0.0
        sse_count = 0

        ## minibatch loop over the dataset
        for i in np.arange(0, Y.shape[0], bsize):
            idx = np.arange(i, np.minimum(i + bsize, Y.shape[0]))
            fd = {}

            self.fd_regularization(fd, train=False)

            ## adding compound inputs
            self.fd_input(fd, self.X, idx)
            fd[self.cmpd_idx] = idx
    
            yi, yv, _             = select_rows(Y, idx)
            fd[self.y_true_idx]   = yi
            fd[self.y_true_value] = yv == 1.0
    
            if self.has_regr():
                yregr_i, yregr_v, _       = select_rows(Yregr, idx)
                fd[self.yregr_true_idx]   = yregr_i
                fd[self.yregr_true_value] = yregr_v
            ## Yaux is not needed because its error is not reported

            logloss_batch, pred_batch, sse_batch, sse_size = self.sess.run(
                    [self.logloss, self.y_sel, self.yregr_sse, self.yregr_size],
                    feed_dict = fd)
            pred_list.append(pred_batch)
            yi_list.append(yi)
            yv_list.append(yv)
            logloss   += logloss_batch
            sse       += sse_batch
            sse_count += sse_size

        meanll = logloss / Y.nnz if Y.nnz > 0 else np.nan

        pred   = np.concatenate(pred_list)
        yis    = np.concatenate(yi_list)
        yvs    = np.concatenate(yv_list)
        mse    = sse / sse_count if sse_count > 0 else np.nan
        return meanll, pred, yis, yvs, mse

    def eval_aucs(self, train, bsize=10000):
        """
        Arguments:
        train    if True then train, otherwise test evaluation
        bsize    minibatch size

        Returns:
        DataFrame with AUCs
        """
        _,  ypred,  yi,  yv, _ = self.eval(train=train, bsize=bsize)

        return comp_aucs_pd(cols=yi[:,1], ytrue=yv, ypred=ypred)

    def fd_regularization(self, fd, train):
        """Adds L2 regularization and dropout to fd.
        train      if True, uses training mode, otherwise testing mode
        """
        if train:
            fd[self.dropout_retain] = self.dropout
        else:
            fd[self.dropout_retain] = 1.0

    def fd_input(self, fd, X, idx=None):
        """Adds minibatch of X given by idx to the fd.
        If idx is None, adds all rows of X."""
        if self.is_dense():
            if idx is None:
                fd[self.x_dense] = X
            else:
                fd[self.x_dense] = X[idx, :]
        else:
            xi, xv, xs         = select_rows(X, idx)
            fd[self.x_indices] = xi
            fd[self.x_value]   = xv
            fd[self.x_shape]   = xs

    def run_epoch(self, lr=None, evaluate=True, verbose=True):
        """ Runs a single epoch.
        lr         learning rate (if lr is None use from self.lr_path)
        evaluate   whether to evaluate AUCs and logloss
        """
        if not lr:
            ep = np.minimum(self.epoch, len(self.lr_path) - 1)
            lr = self.lr_path[ep]
        idx_train  = np.random.permutation(np.arange(self.Ytrain.shape[0]))
        batch_nrow = self.batch_num_rows
        start_list = np.arange(0, idx_train.shape[0] - batch_nrow + 1, batch_nrow)

        t0 = time.time()

        for start in tqdm(start_list, leave=False, disable=not verbose):
            idx = idx_train[start : (start + batch_nrow)]
            fd  = {}
            fd[self.learning_rate]  = lr
            fd[self.dropout_retain] = self.dropout
            fd[self.cmpd_idx]       = idx
            fd[self.num_batches]    = start_list.shape[0]

            self.fd_input(fd, self.X, idx)

            yi, yv, _               = select_rows(self.Ytrain, idx)
            fd[self.y_true_idx]     = yi
            fd[self.y_true_value]   = yv == 1.0

            if self.has_regr():
                ## feeding regression data
                yri, yrv, _               = select_rows(self.Yregr, idx)
                fd[self.yregr_true_idx]   = yri
                fd[self.yregr_true_value] = yrv

            if self.has_yaux():
                ## feeding y_prime_idx and y_prime_value
                ypi, ypv, _            = select_rows(self.Yaux, idx)
                fd[self.y_prime_idx]   = ypi
                fd[self.y_prime_value] = ypv

            self.sess.run(self.train_op, feed_dict=fd)

        if evaluate:
            logloss_train, pred_train, yi_train, yv_train, mse_train = self.eval(train=True)
            logloss_test,  pred_test,  yi_test,  yv_test,  mse_test  = self.eval(train=False)

            rmse_train = np.sqrt(mse_train)
            rmse_test  = np.sqrt(mse_test)

            auc_train = comp_aucs_pd(
                    cols     = yi_train[:,1],
                    ytrue    = yv_train,
                    ypred    = pred_train)

            auc_test = comp_aucs_pd(
                    cols     = yi_test[:,1],
                    ytrue    = yv_test,
                    ypred    = pred_test)

            t1 = time.time()

            auc_train_mean = auc_train.loc[self.auc_cols].mean()
            auc_test_mean  = auc_test.loc[self.auc_cols].mean()
            if type(auc_train_mean) not in [np.float64, np.float32]:
                auc_train_mean = np.nan
            if type(auc_test_mean) not in [np.float64, np.float32]:
                auc_test_mean = np.nan

            self.aucs_train_all = auc_train
            self.aucs_test_all  = auc_test

            self.aucs_train.append(auc_train_mean)
            self.aucs_test.append(auc_test_mean)
            self.loglosses_train.append(logloss_train)
            self.loglosses_test.append(logloss_test)
            self.rmses_train.append(rmse_train)
            self.rmses_test.append(rmse_test)

            if self.board:
               self.train_writer.add_summary(make_summary('logloss', logloss_train), self.epoch)
               self.test_writer.add_summary( make_summary('logloss', logloss_test), self.epoch)

               self.train_writer.add_summary(make_summary('RMSE', rmse_train), self.epoch)
               self.test_writer.add_summary( make_summary('RMSE', rmse_test), self.epoch)

               self.train_writer.add_summary(make_summary('AUC', auc_train_mean), self.epoch)
               self.test_writer.add_summary( make_summary('AUC', auc_test_mean), self.epoch)

            if (self.epoch % 20) == 0:
                if self.has_regr():
                    head = "\tlogl_tr  logl_te |  auc_tr   auc_te | rmse_tr  rmse_te "
                else:
                    head = "\tlogl_tr  logl_te |  auc_tr   auc_te"

                if self.verbose:
                    print(head)
                else:
                    logging.info(head)

            log = "%d." % self.epoch
            log += "\t%.5f  %.5f" % (logloss_train, logloss_test)
            log += " | %.5f  %.5f" % (auc_train_mean, auc_test_mean)
            if self.has_regr():
                log += " | %.5f  %.5f" % (rmse_train, rmse_test)
            log += " | %.1e [%.1f sec]" % (lr, t1 - t0)

            if self.verbose:
                print(log)
            else:
                logging.info(log)
        else:
            ## no evaluation
            t1 = time.time()
            log = "%d. [%.1f sec]" % (self.epoch, t1 - t0)
            if self.verbose:
                print(log)
            else:
                logging.info(log)

        ## performing epoch += 1
        self.sess.run(self.incr_global_step)


    def run(self, epochs=None, evaluate=True, verbose=True):
        """
        epochs     number of epochs to run (None means run till the end of lr_path)
        evaluate   whether to evalaute AUCs / logloss
        """
        if not epochs:
            ## running till the end
            epochs = len(self.lr_path) - self.epoch
            if epochs <= 0:
                epochs = 1
        for i in range(epochs):
            self.run_epoch(evaluate=evaluate, verbose=verbose)

    def get_aucs(self):
        df = pd.DataFrame(index = np.arange(self.Ytrain.shape[1]))
        df["aucs_train"] = self.aucs_train_all
        df["aucs_test"]  = self.aucs_test_all
        df["num_pos"]    = np.array(self.num_pos).flatten()
        df["num_neg"]    = np.array(self.num_neg).flatten()
        df["col"]        = df.index
        return df

    def extend(self, X, Y, Ytest = None, **kwargs):
        """Same named arguments as when constructing ExtendedSMLP
        """
        return ExtendedSMLP(smlp=self, X=X, Y=Y, Ytest=Ytest, **kwargs)


class ExtendedSMLP(object):
    def __init__(self,
                 smlp,
                 X,
                 Y,
                 Ytest       = None,
                 auc_min_pos = 50,
                 auc_min_neg = 50,
                 auc_cols    = None,
                 nonlin      = "relu",
                 h_sizes     = None,
                 dropout     = 1.0,
                 regul       = 1e-3,
                 optimizer   = "adam",
                 lr          = [1e-3, 1e-4],
                 lr_durations= [50, 50],
                 batch_size  = 0.01,
                 board       = None):
        """ Creates extended SparseMLP where new assays are added.
        smlp            existing SparseMLP
        X               matrix of input features (either dense or sparse)
        Y               sparse matrix with new columns (assays)
        Ytest           sparse matrix with test data, same shape as Y.
                        or None (no test data)
                        or Boolean vector, assigning specific rows to test set
        auc_min_pos     number of positives of column for calculating AUC
        auc_min_neg     number of negatives of column for calculating AUC
        auc_cols        columns to compute AUC on (if specified then auc_min_pos, auc_min_neg are not used)
        nonlin          non-linearity ("relu", "relu6", "elu", "tanh") new neurons
        h_sizes         additional neurons (a list), separate tower (None if no)
        dropout         dropout retain probability (for the new neurons)
        regul           scalar (for l2 regularization)
                        or tf.contrib.layers.l1/l2/l1_l2_regularizer
        optimizer       optimizer ("adam", "sgd", "rmsprop")
        lr              list of learning rates
        lr_durations    list of learning rate durations
        batch_size      size of minibatches, either an int or a float.
                        In the latter case gives the relative size
        board           board directory (None means no board)
        """
        self.smlp = smlp

        if self.smlp.has_embedding():
            raise ValueError("Extending SparseMLP with embedding not supported.")

        if X.shape[0] != Y.shape[0]:
            raise ValueError("X and Y must have the same number of rows.")
        if X.shape[1] != self.smlp.X.shape[1]:
            raise ValueError("X must have the same number of columns as smlp.X.")
        if type(X) == np.ndarray and not self.smlp.is_dense():
            raise ValueError("smlp.X is dense matrix, X must be dense too.")
        
        if type(X) == np.ndarray:
            self.X = X
        else:
            self.X = X.tocsr(copy=False)


        #########   handling Y   ###########
        Y = to1encoding(Y)
        if Y is None:
            raise ValueError("Y must contain only booleans or 1.0 / -1.0 values.")
        ## no test data
        if Ytest is None:
            Ytest = csr_matrix(self.Y.shape)
        ## assigning specific rows to the test set
        if type(Ytest) == np.ndarray:
            if Y.shape[0] != len(Ytest):
                raise ValueError("Number of rows in Y must equal the length of Ytest")
            Ytrain, Ytest = make_train_test_rows(Y, Ytest)
        elif type(Ytest) in [scipy.sparse.coo.coo_matrix, scipy.sparse.csr.csr_matrix, scipy.sparse.csc.csc_matrix]:
            Ytrain = Y
            Ytest  = to1encoding(Ytest)
        else:
            raise TypeError("Ytest must be either scipy sparse matrix or boolean vector or None.")
        if Y.shape != Ytest.shape:
            raise ValueError("Y and Ytest must have the same shape.")

        self.Ytrain = Ytrain.tocsr()
        self.Ytest  = Ytest.tocsr()


        ######## batch_size ########
        if type(batch_size) == int:
            self.batch_num_rows = batch_size
        elif type(batch_size) == float:
            assert batch_size <= 1.0
            self.batch_num_rows = np.int(np.ceil(batch_size * self.Ytrain.shape[0]))
        else:
            raise TypeError("batch_size must an integer or float.")


        ########  AUC  #######
        self.num_pos = (self.Ytest == 1).sum(0) + (self.Ytrain == 1).sum(0)
        self.num_neg = (self.Ytest ==-1).sum(0) + (self.Ytrain ==-1).sum(0)
        if auc_cols is None:
            self.auc_cols = np.array(np.where(
                (self.num_pos >= auc_min_pos) &
                (self.num_neg >= auc_min_neg)
            )[1]).flatten()
            logging.info("Computing mean AUC over %d columns (with %d positives and %d negatives)." %
                         (len(self.auc_cols), auc_min_pos, auc_min_neg))
        else:
            self.auc_cols = auc_cols
            logging.info("Using the provided %d columns for computing the mean AUC." % len(self.auc_cols))


        #######  model  #######
        nonlins = {"relu":  tf.nn.relu,
                   "relu6": tf.nn.relu6,
                   "elu":   tf.nn.elu,
                   "tanh":  tf.tanh,
                   "swish": lambda x: x * tf.sigmoid(x)}
        if nonlin not in nonlins:
            raise ValueError("nonlin must be one of %s." % nonlins.keys())

        self.dropout    = dropout
        if isinstance(regul, numbers.Number):
            self.reg_layer = tf.contrib.layers.l2_regularizer(regul)
        elif callable(regul):
            self.reg_layer = regul
        else:
            raise ValueError("Input 'regul' must be either number (L2 regul) or tf.contrib.layers.l1/l2/l1_l2_regularizer specifying the regularization")
        self.optimizer  = optimizer
        self.lr_path    = np.repeat(lr, lr_durations)

        self.nonlin     = nonlins[nonlin]
        self.h_sizes    = h_sizes

        self.board           = board
        self.aucs_train      = []
        self.aucs_test       = []
        self.loglosses_train = []
        self.loglosses_test  = []
        self.rmses_train     = []
        self.rmses_test      = []

        self.setup_tf()

    @property
    def epoch(self):
        return self.sess.run(self.global_step)

    def setup_tf(self):
        with tf.variable_scope(None, default_name="ExtendedSMLP"):
            self.var_scope      = tf.get_variable_scope().name

            ## global step (epoch number)
            self.global_step      = tf.Variable(0,
                                                dtype     = tf.int32,
                                                name      = 'global_step',
                                                trainable = False)
            self.incr_global_step = tf.assign(self.global_step, self.global_step + 1)

            self.learning_rate  = tf.placeholder(tf.float32, name="learning_rate")
            self.dropout_retain = tf.placeholder(tf.float32, name="dropout_retain")
            self.y_true_idx     = tf.placeholder(tf.int64, shape=[None, 2], name="y_true_idx")
            self.y_true_value   = tf.placeholder(tf.float32, shape=[None], name="y_true_value")
            self.num_batches    = tf.placeholder(tf.float32, shape=(), name="num_batches")
            with tf.variable_scope("tower"):
                if self.h_sizes:
                    ## creating a new tower
                    self.last_hidden_new = self.smlp.make_tower(
                        h_sizes = self.h_sizes,
                        nonlin  = self.nonlin,
                        regul   = self.reg_layer,
                        dropout = self.dropout_retain,
                        embed   = False
                    )
                else:
                    self.last_hidden_new = None


            with tf.variable_scope("output"):
                hsize  = self.smlp.h_sizes[-1]
                osize  = self.Ytrain.shape[1]

                ## adding tower to the SMLP last hidden
                if self.last_hidden_new is None:
                    hdo = self.smlp.last_hidden
                else:
                    hdo = tf.concat([self.smlp.last_hidden, self.last_hidden_new], axis = 1)
                    hsize += self.h_sizes[-1]

                Wo     = tf.get_variable("Wo", shape=[hsize, osize], dtype=tf.float32, regularizer = self.reg_layer)
                bo     = tf.get_variable("bo", shape=[osize], dtype=tf.float32, initializer = tf.zeros_initializer())
                self.y = tf.nn.bias_add(tf.matmul(hdo, Wo), bo, name="y")

            with tf.variable_scope("loss"):
                y_sel    = tf.gather_nd(self.y, self.y_true_idx, name="y_sel")
                logloss  = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=y_sel, labels=self.y_true_value))

                reg_term = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES, self.var_scope)
                ## regularization is split per batch
                loss     = logloss + sum(reg_term) / self.num_batches

                self.logloss = logloss
                self.y_sel   = y_sel

            ## adding optimizer
            if self.optimizer =="adam":
                opt = tf.train.AdamOptimizer(self.learning_rate)
            elif self.optimizer == "sgd":
                opt = tf.train.GradientDescentOptimizer(self.learning_rate)
            elif self.optimizer == "rmsprop":
                opt = tf.train.RMSPropOptimizer(self.learning_rate)
            else:
                raise ValueError("Undefined optimizer: %s" % self.optimizer)

            evars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.var_scope)
            self.train_op = opt.minimize(loss, var_list=evars)
            logging.info("TensorFlow model setup completed for Extended model!")

            self.sess = self.smlp.sess
            if self.board:
                self.train_writer = tf.summary.FileWriter(self.board + "/train", self.sess.graph)
                self.test_writer  = tf.summary.FileWriter(self.board + "/test")

            ivars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.var_scope)
            self.sess.run(tf.variables_initializer(ivars))


    def run_epoch(self, lr=None, evaluate=True, verbose=True):
        """ Runs a single epoch for extended model.
        lr        learning rate (if lr is None use from self.lr_path)
        evaluate  whether to evaluate AUCs and logloss
        """
        if not lr:
            ep = np.minimum(self.epoch, len(self.lr_path))
            lr = self.lr_path[ep]
        idx_train  = np.random.permutation(np.arange(self.Ytrain.shape[0]))
        batch_nrow = self.batch_num_rows
        start_list = np.arange(0, idx_train.shape[0] - batch_nrow + 1, batch_nrow)

        t0 = time.time()

        for start in tqdm(start_list, leave=False, disable=not verbose):
            idx = idx_train[start : (start + batch_nrow)]
            fd  = {}
            fd[self.learning_rate]  = lr
            fd[self.dropout_retain] = self.dropout
            fd[self.num_batches]    = start_list.shape[0]

            ## adding variables from the primary model
            self.smlp.fd_regularization(fd, train=True)
            self.smlp.fd_input(fd, self.X, idx)

            yi, yv, _             = select_rows(self.Ytrain, idx)
            fd[self.y_true_idx]   = yi
            fd[self.y_true_value] = yv == 1.0

            self.sess.run(self.train_op, feed_dict=fd)

        if evaluate:
            ll_train, pred_train, yi_train, yv_train = self.eval(train=True)
            ll_test,  pred_test,  yi_test,  yv_test  = self.eval(train=False)

            auc_train = comp_aucs_pd(
                    cols     = yi_train[:,1],
                    ytrue    = yv_train,
                    ypred    = pred_train)

            auc_test = comp_aucs_pd(
                    cols     = yi_test[:,1],
                    ytrue    = yv_test,
                    ypred    = pred_test)

            auc_train_mean = auc_train.loc[self.auc_cols].mean()
            auc_test_mean  = auc_test.loc[self.auc_cols].mean()

            self.aucs_train_all = auc_train
            self.aucs_test_all  = auc_test

            self.aucs_train.append(auc_train_mean)
            self.aucs_test.append(auc_test_mean)
            self.loglosses_train.append(ll_train)
            self.loglosses_test.append(ll_test)

            t1 = time.time()

            if self.board:
               self.train_writer.add_summary(make_summary('logloss', ll_train), self.epoch)
               self.test_writer.add_summary( make_summary('logloss', ll_test), self.epoch)

               self.train_writer.add_summary(make_summary('AUC', auc_train_mean), self.epoch)
               self.test_writer.add_summary( make_summary('AUC', auc_test_mean), self.epoch)
            log = "%d." % self.epoch
            log += "\t%.5f  %.5f" % (ll_train, ll_test)
            log += " | %.5f  %.5f" % (auc_train_mean, auc_test_mean)
            log += " | %.1e [%.1f sec]" % (lr, t1 - t0)

            logging.info(log)
        else:
            ## no evaluation
            t1 = time.time()
            logging.info("%d. [%.1f sec]" % (self.epoch, t1 - t0))

        ## performing epoch += 1
        self.sess.run(self.incr_global_step)


    def predict(self, X):
        """Makes full predictions for the given input matrix X for all Y columns."""
        if X.shape[1] != self.X.shape[1]:
            raise ValueError("X.shape[1] (%d) must be equal to original feature size %d." % (X.shape[1], self.X.shape[1]))

        fd = {}
        fd[self.dropout_retain] = 1.0

        self.smlp.fd_regularization(fd, train=False)
        self.smlp.fd_input(fd, X, idx=None)

        return self.sess.run(self.y, feed_dict=fd)


    def eval(self, train, bsize = 10000):
        """
        Arguments:
        train    if True then train, otherwise test evaluation
        bsize    minibatch size

        Returns:
        mean logloss
        predictions
        y coordinates (N by 2 matrix)
        y values
        """
        if train:
            Y = self.Ytrain
        else:
            Y = self.Ytest

        logloss = 0.0
        pred_list = []
        yi_list = []
        yv_list = []

        ## minitach loop over the dataset
        for i in np.arange(0, Y.shape[0], bsize):
            idx = np.arange(i, np.minimum(i + bsize, Y.shape[0]))
            fd  = {}
            fd[self.dropout_retain] = 1.0

            ## adding variables from the primary model
            self.smlp.fd_regularization(fd, train=False)
            self.smlp.fd_input(fd, self.X, idx)

            yi, yv, _             = select_rows(Y, idx)
            fd[self.y_true_idx]   = yi
            fd[self.y_true_value] = yv == 1.0

            logloss_batch, pred_batch = self.sess.run(
                    [self.logloss, self.y_sel],
                    feed_dict = fd)
            pred_list.append(pred_batch)
            yi_list.append(yi)
            yv_list.append(yv)
            logloss   += logloss_batch

        ## computing aggregate performance
        meanll = logloss / Y.nnz
        pred   = np.concatenate(pred_list)
        yis    = np.concatenate(yi_list)
        yvs    = np.concatenate(yv_list)

        return meanll, pred, yis, yvs

    def eval_aucs(self, train, bsize):
        """
        Arguments:
        train    if True then train, otherwise test evaluation
        bsize    minibatch size

        Returns:
        DataFrame with AUCs
        """
        _,  ypred,  yi,  yv = self.eval(train=train, bsize=bsize)

        return comp_aucs_pd(cols=yi[:,1], ytrue=yv, ypred=ypred)

    def run(self, epochs=None, evaluate=True, verbose=True):
        """
        epochs     number of epochs to run (None means run till the end of lr_path)
        evaluate   whether to evalaute AUCs / logloss
        """
        if not epochs:
            ## running till the end
            epochs = len(self.lr_path) - self.epoch
            if epochs <= 0:
                epochs = 1
        for i in range(epochs):
            self.run_epoch(evaluate=evaluate, verbose=verbose)

    def get_aucs(self):
        df = pd.DataFrame(OrderedDict([
            ("aucs_train", self.aucs_train_all),
            ("aucs_test", self.aucs_test_all),
            ("num_pos", np.array(self.num_pos).flatten()),
            ("num_neg", np.array(self.num_neg).flatten()),
        ]))
        df["col"] = df.index
        return df
