import unittest
import sparseflow.auc
import scipy.sparse
import numpy as np

class TestAUC(unittest.TestCase):
    def test_auc(self):
        ## generating data
        cols  = np.repeat(np.arange(10), 50)
        N     = np.int(cols.shape[0] / 2)
        ytrue = np.column_stack([np.zeros(N), np.ones(N)]).flatten()
        ypred = np.random.randn(ytrue.shape[0])

        a1 = sparseflow.auc.comp_aucs_pd(cols, ytrue=ytrue, ypred=ypred)
        a2 = sparseflow.auc.comp_aucs(cols, ytrue=ytrue, ypred=ypred, auc_cols=np.arange(10))

        self.assertTrue( np.allclose(a1, a2) )

if __name__ == '__main__':
    unittest.main()
