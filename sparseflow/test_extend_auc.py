import scipy.sparse
import numpy as np
import sparseflow

X = np.sign(np.random.randn(100, 5))
w = np.random.randn(5, 2)
Y = scipy.sparse.csr_matrix(X.dot(w))
Y.data = Y.data > 0.0


test = np.arange(Y.shape[0]) < (0.2 * Y.shape[0])

smlp = sparseflow.SparseMLP(Y=Y, Ytest=test, X=X, h_sizes=[4], lr=[1e-3, 3e-4], lr_durations=[20, 10], regul=1.0, auc_min_pos=30, auc_min_neg=30)
smlp.run()

print(smlp.get_aucs())

idx   = np.random.permutation(X.shape[0])
X2    = X[idx,:]
Y2    = Y[idx,:]
test2 = test[idx]

for rep in range(5):
    esmlp  = smlp.extend(X = X, Y = Y, Ytest = test, lr=[1e-3, 1e-4], lr_durations=[20, 10], auc_min_pos=30, auc_min_neg=30, regul=1.0)
    esmlp.run()

    print(esmlp.get_aucs())
