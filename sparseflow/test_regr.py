import unittest
import sparseflow
import scipy.sparse
import tensorflow as tf
import numpy as np
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

x = np.random.randn(300, 2)
y = scipy.sparse.csr_matrix(x)
y.data = y.data > 0.5

test = np.random.rand(y.shape[0]) < 0.5

#smlp = sparseflow.SparseMLP(Y=y, Ytest=test, X=x, h_sizes=[2], lr=[1e-2], lr_durations=[20], auc_min_pos=5, auc_min_neg=5)


smlp = sparseflow.SparseMLP(Y=y, Ytest=test, X=x, Yregr=x, Yregr_test=test, Yregr_weight=2.5, h_sizes=[2], lr=[1e-2], lr_durations=[100], auc_min_pos=5, auc_min_neg=5)
smlp.run()
smlp.rmses_train
smlp.rmses_test
smlp.aucs_train
smlp.aucs_test

