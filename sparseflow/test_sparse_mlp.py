import unittest
import sparseflow
import scipy.sparse
import tensorflow as tf
import numpy as np

class TestSMLP(unittest.TestCase):
    def setUp(self):
    	tf.reset_default_graph()

    def test_mlp_basic(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30, batch_size=0.1)
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)
        self.assertEqual(smlp.get_hidden_train().shape, (300, 8))

        yhat1 = smlp.get_y_train()
        self.assertEqual(yhat1.shape, (300, 27))
        self.assertEqual(smlp.has_embedding(), False)

        yhat2 = smlp.predict(ecfp.tocsr()[0:-1, :].tocoo())
        self.assertEqual(yhat2.shape, (299, 27))
        self.assertTrue(np.allclose(yhat1[0:-1, :], yhat2, atol=1e-4))

        ## testing only evaluating AUCs
        aucs_only = smlp.eval_aucs(train=False)
        self.assertTrue(np.allclose(smlp.aucs_test[-1], aucs_only.mean(), atol=1e-4))


        smlp2 = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8, 10], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30, batch_size=0.1)
        smlp2.run()

        self.assertEqual(smlp2.epoch, 5)
        self.assertEqual(smlp2.get_hidden_train().shape, (300, 10))
        self.assertEqual(smlp2.get_y_train().shape, (300, 27))

        ## testing l1 regularization
        smlp3 = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8, 10], lr=[1e-3], lr_durations=[2], regul=tf.contrib.layers.l1_regularizer(0.5), batch_size=0.1)
        smlp3.run()

        self.assertEqual(smlp3.epoch, 2)
        self.assertEqual(smlp2.get_hidden_train().shape, (300, 10))
        self.assertEqual(smlp2.get_y_train().shape, (300, 27))

    def test_no_test(self):
        ## allow empty Ytest
        Y = scipy.sparse.random(20, 10, 0.5)
        Y.data = Y.data > 0.5
        smlp4 = sparseflow.SparseMLP(
                Y            = Y,
                Ytest        = None,
                X            = np.random.randn(20, 8).astype(np.float32),
                h_sizes      = [4],
                lr           = [1e-3],
                lr_durations = [2],
                batch_size   = 0.2)
        smlp4.run()

    def test_empty_columns(self):
        ## generating data
        Y1 = scipy.sparse.random(30, 10, 0.5)
        Y2 = scipy.sparse.random(30, 10, 0.5)
        Y = scipy.sparse.hstack([Y1, scipy.sparse.csr_matrix( (30, 3) ), Y2])
        Y.data = Y.data > 0.5
        X = np.random.randn(30, 2)

        test = np.arange(Y.shape[0]) < 15

        smlp = sparseflow.SparseMLP(Y=Y, Ytest=test, X=X, h_sizes=[2], lr=[1e-3], lr_durations=[2], auc_min_pos=2, auc_min_neg=2, batch_size=2)
        smlp.run()
        aucs = smlp.get_aucs()

        self.assertEqual(aucs.shape[0], Y.shape[1])
        self.assertTrue( np.all(aucs["num_pos"].iloc[10:13] == [0, 0, 0]) )
        self.assertTrue( np.all(aucs["num_neg"].iloc[10:13] == [0, 0, 0]) )
        self.assertTrue( np.isnan(aucs["aucs_train"].iloc[10:13]).all() )
        self.assertTrue( np.isnan(aucs["aucs_test"].iloc[10:13]).all() )

    def test_ytest_matrix(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)
        yt        = scipy.sparse.random(300, 27, 0.25)
        yt.data   = (yt.data > 0.5)

        ## with both Y and Ytest specified
        smlp1 = sparseflow.SparseMLP(Y=ic50, Ytest=yt, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 1])
        smlp1.run()

        yt2   = scipy.sparse.random(300, 27, 0.25)
        with self.assertRaises(ValueError):
            smlp2 = sparseflow.SparseMLP(Y=ic50, Ytest=yt2, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 1])

    def test_input_block(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8], lr=[1e-3], lr_durations=[2], input_block=3)
        smlp.run()

    def test_regression(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        yregr = scipy.sparse.random(300, 5, 0.5).tocsr()

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, Yregr = yregr, Yregr_test = test, Yregr_weight = 0.1, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30, batch_size = 0.1)
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)

        self.assertEqual(len(smlp.rmses_train), 5)
        self.assertEqual(len(smlp.rmses_test), 5)
        self.assertTrue( (np.array(smlp.rmses_train) >= 0.0).all() )
        self.assertTrue( (np.array(smlp.rmses_test) >= 0.0).all() )

        ## testing regression without classification
        smlp = sparseflow.SparseMLP(Y=None, Ytest=None, Yregr = yregr, Yregr_test = test, Yregr_weight = 0.1, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30, batch_size = 0.1, verbose=True)
        smlp.run()
        yhat = smlp.predict_regr(ecfp)
        self.assertEqual(yregr.shape, yhat.shape)

        ## full training set
        yfull = smlp.get_y_regr_train()
        self.assertEqual(yregr.shape, yfull.shape)

        ## test set dataframe
        ytest_df = smlp.get_y_regr_testset()
        self.assertEqual(ytest_df.shape[0], yregr[test].nnz)
        row = ytest_df.row.iloc[0]
        col = ytest_df.col.iloc[0]
        self.assertTrue(np.allclose(ytest_df.y_hat.iloc[0], yfull[row, col]))

        ## trying empty Yregr_test
        smlp2 = sparseflow.SparseMLP(Yregr = yregr, Yregr_test = None, Yregr_weight = 1.0, X=ecfp, h_sizes=[8], lr=[1e-3], lr_durations=[2], batch_size = 0.1, verbose=True)
        smlp2.run()

        ## NA value
        yregr.data[[0, 2]] = np.nan
        smlp3 = sparseflow.SparseMLP(Yregr = yregr, Yregr_test = None, Yregr_weight = 1.0, X=ecfp, h_sizes=[8], lr=[1e-3], lr_durations=[2], batch_size = 0.1, verbose=True)
        smlp3.run()
        self.assertTrue(np.isnan(smlp3.rmses_train[-1]) == False)


    def test_yprime(self):
        ## generating data
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ic50.data = (ic50.data > 0.5)
        yprime = scipy.sparse.random(300, 10, 0.3)
        yprime.data = (yprime.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, Yprime=yprime, Yprime_weight=0.2,
                h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30)
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)
        self.assertEqual(smlp.get_hidden_train().shape, (300, 8))
        self.assertEqual(smlp.get_y_train().shape, (300, 27))

    def test_yaux(self):
        ## generating data
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ic50.data = (ic50.data > 0.5)
        yaux = scipy.sparse.random(300, 10, 0.3)
        yaux.data = (yaux.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, Yaux=yaux, Yaux_weight=0.2,
                h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30)
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)
        self.assertEqual(smlp.get_hidden_train().shape, (300, 8))
        self.assertEqual(smlp.get_y_train().shape, (300, 27))

    def test_mlp_dense(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ic50.data = (ic50.data > 0.5)
        ecfp = np.random.randn(300, 50)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30)
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)
        self.assertEqual(smlp.has_embedding(), False)

        ## check if get_y_train and predict
        yhat1 = smlp.get_y_train()
        self.assertEqual(yhat1.shape, (300, 27))

        yhat2 = smlp.predict(ecfp[0:-1, :])
        self.assertEqual(yhat2.shape, (299, 27))
        self.assertTrue(np.allclose(yhat1[0:-1, :], yhat2, atol=1e-4))

        smlp2 = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8, 10], lr=[1e-3, 1e-4], lr_durations=[2, 3], auc_min_pos=30, auc_min_neg=30)
        smlp2.run()

        self.assertEqual(smlp2.epoch, 5)

    def test_embedding(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8], lr=[1e-3, 1e-4], lr_durations=[2, 3], emb_sizes=[8])
        smlp.run()
        self.assertEqual(smlp.epoch, 5)
        self.assertEqual(len(smlp.aucs_train), 5)
        self.assertEqual(len(smlp.aucs_test), 5)
        self.assertEqual(smlp.get_hidden_train().shape, (300, 8))
        self.assertEqual(smlp.get_y_train().shape, (300, 27))
        self.assertEqual(smlp.has_embedding(), True)

    def test_extending(self):
        ## generating data
        ic50 = scipy.sparse.random(300, 27, 0.5)
        ecfp = scipy.sparse.random(300, 50, 0.15)
        ic50.data = (ic50.data > 0.5)

        test = np.random.rand(ic50.shape[0]) < 0.2

        smlp = sparseflow.SparseMLP(Y=ic50, Ytest=test, X=ecfp, h_sizes=[8], lr=[1e-3], lr_durations=[1], auc_min_pos=30, auc_min_neg=30)
        smlp.run()

        yhat0a = smlp.get_y_train()

        ## data for extended model
        e_ic50 = scipy.sparse.random(100, 2,  0.5)
        e_ecfp = scipy.sparse.random(100, 50, 0.15)
        e_ic50.data = (e_ic50.data > 0.5)

        e_test = np.random.rand(e_ic50.shape[0]) < 0.5
        esmlp  = smlp.extend(X = e_ecfp, Y = e_ic50, Ytest = e_test, lr=[1e-3, 1e-4], lr_durations=[1, 2])

        esmlp.run()

        self.assertEqual(esmlp.epoch, 3)

        yhat0b = smlp.get_y_train()
        self.assertTrue( np.allclose(yhat0a, yhat0b, atol=1e-4) )

        ## predicting
        yhat1 = esmlp.predict(e_ecfp.tocsr()[0:80, :])
        self.assertEqual(yhat1.shape, (80, 2))

        mll, pred, yi, yv = esmlp.eval(train = False)
        self.assertEqual(yi.shape[0], e_ic50.tocsr()[e_test,:].nnz)

        mll, pred, yi, yv = esmlp.eval(train = True)
        self.assertEqual(yi.shape[0], e_ic50.tocsr()[e_test==False,:].nnz)

        ## aucs
        aucs = esmlp.get_aucs()
        self.assertEqual(aucs.shape[0], e_ic50.shape[1])

        ##### extending with extra tower #####
        esmlp2 = smlp.extend(X = e_ecfp, Y = e_ic50, Ytest = e_test, lr=[1e-3, 1e-4], lr_durations=[1, 2], h_sizes = [2, 4], dropout=0.9, regul=2e-3)
        esmlp2.run()

        ## checking that SMLP is correct
        yhat0c = smlp.get_y_train()
        self.assertTrue( np.allclose(yhat0a, yhat0c, atol=1e-4) )

if __name__ == '__main__':
    unittest.main()
