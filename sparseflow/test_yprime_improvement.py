import unittest
import sparseflow
import scipy.sparse
import tensorflow as tf
import numpy as np

import logging

class TestYPrime(unittest.TestCase):

    def test_yprime_improvement(self):
        Ntrain = 100
        Ntest  = 400

        Xdim = 50
        Ydim = 2
        ## generating data
        #ecfp = scipy.sparse.random(Ntrain + Ntest, Xdim, 0.25)
        ecfp = np.random.randn(Ntrain + Ntest, Xdim)
        #W = np.random.randn(Xdim, Ydim)
        W = np.random.randint(0, 2, size=(Xdim, Ydim)) * 2.0 - 1.0
        ic50_raw = 1.0 / (1.0 + np.exp(-ecfp.dot(W)))
        ic50 = scipy.sparse.csr_matrix(ic50_raw)
        ic50.data = ic50.data > 0.5

        yprime = ic50.copy()
        yprime.data = yprime.data + 0.0

        test = np.zeros(Ntrain + Ntest, dtype=np.bool)
        test[-Ntest:] = True

        ## params
        lr = [1e-2, 1e-3, 3e-4]
        lr_durations = [20, 20, 5]
        dropout = 0.8
        l2_reg = 2e-2

        smlp = sparseflow.SparseMLP(
                Y      = ic50,
                Ytest  = test,
                X      = ecfp,
                Yprime = yprime,
                Yprime_weight = 1.0,
                h_sizes = [4],
                lr = lr,
                lr_durations = lr_durations,
                auc_min_pos  = 10,
                auc_min_neg  = 10,
                dropout = dropout,
                l2_reg = l2_reg)
        smlp.run()

        print("Yprime AUCS(train): %s" % smlp.aucs_train_all)
        print("Yprime AUCS(test):  %s" % smlp.aucs_test_all)

        tf.reset_default_graph()

        ## without yprime
        smlp0 = sparseflow.SparseMLP(
                Y      = ic50,
                Ytest  = test,
                X      = ecfp,
                h_sizes = [4],
                lr = lr,
                lr_durations = lr_durations,
                auc_min_pos  = 10,
                auc_min_neg  = 10,
                dropout      = dropout,
                l2_reg       = l2_reg)
        smlp0.run()
        print("Base AUCS(train): %s" % smlp0.aucs_train_all)
        print("Base AUCS(test):  %s" % smlp0.aucs_test_all)

        print("=======")
        print("Delta AUCS(train): %s" % (smlp.aucs_train_all - smlp0.aucs_train_all))
        print("Delta AUCS(test):  %s" % (smlp.aucs_test_all - smlp0.aucs_test_all))

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
    unittest.main()
